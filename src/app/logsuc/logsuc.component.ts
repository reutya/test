import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-logsuc',
  templateUrl: './logsuc.component.html',
  styleUrls: ['./logsuc.component.css']
})
export class LogsucComponent implements OnInit {
  userId:string;
  email:string;

  constructor(private router:Router ,private route:ActivatedRoute,public authService :AuthService) { }

  ngOnInit() {
     // this.id=this.route.snapshot.params.id;
     this.authService.user.subscribe(
      user=>{
        this.userId = user.uid;
        this.email=user.email;
        
      }
    )
    console.log(this.userId);
  }

}
