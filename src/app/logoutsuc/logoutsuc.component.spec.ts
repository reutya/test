import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoutsucComponent } from './logoutsuc.component';

describe('LogoutsucComponent', () => {
  let component: LogoutsucComponent;
  let fixture: ComponentFixture<LogoutsucComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoutsucComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutsucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
