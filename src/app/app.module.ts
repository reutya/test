import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {MatExpansionModule} from '@angular/material/expansion';


import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { FormsModule }   from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';


import { RouterModule, Routes } from '@angular/router';


import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage'; 
import { environment } from '../environments/environment';

import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { LogsucComponent } from './logsuc/logsuc.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ClassifiedComponent } from './classified/classified.component';
import { DocformComponent } from './docform/docform.component';
import { LogoutsucComponent } from './logoutsuc/logoutsuc.component';
import { BooksComponent } from './books/books.component';

import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts.service';
//import { ClistComponent } from './clist/clist.component';
//import { EditbookComponent } from './editbook/editbook.component';



const appRoutes: Routes = [
  { path: 'posts', component: PostsComponent },

  { path: 'books', component: BooksComponent },
 
  { path: 'classified', component: ClassifiedComponent},
  //{ path: 'edit/:id', component: EditbookComponent},
  //{ path: 'list', component: ClistComponent},
  { path: 'wel', component: WelcomeComponent},
 { path: 'classify', component: DocformComponent},
  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent},
  { path: 'suc', component: LogsucComponent},
  { path: 'logoutsuc', component: LogoutsucComponent},

  { path: '',
    redirectTo: '/wel',
    pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    LogsucComponent,
    WelcomeComponent,
    ClassifiedComponent,
    DocformComponent,
    LogoutsucComponent,
    BooksComponent,
  
    PostsComponent,
    //ClistComponent,
    //EditbookComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    FormsModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    BrowserModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,    


    RouterModule.forRoot(appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
  ],
  providers: [AngularFireAuth,AngularFirestore ,PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
