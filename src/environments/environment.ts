// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBKgvcEoVbnUSSf2SCvTbzcxg3N8EMMcn4",
  authDomain: "test-ffb97.firebaseapp.com",
  databaseURL: "https://test-ffb97.firebaseio.com",
  projectId: "test-ffb97",
  storageBucket: "test-ffb97.appspot.com",
  messagingSenderId: "493008707995",
  appId: "1:493008707995:web:2aca0861e91ae6ff74413e",
  measurementId: "G-573TCET9DK"
  }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
