import { Component, OnInit } from '@angular/core';
import { Posts } from './../interfaces/posts';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { Comments } from '../interfaces/comments';
import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Router, ActivatedRoute } from '@angular/router';





@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts$: Posts;
  comments$: Comments;
  //posts$:  Observable<Posts>;
  //posts:any;
  userId:string;
  isAdd:boolean=false;
  feed:string="";
  likes:number;
  constructor(private booksservice:BooksService,private route: ActivatedRoute,private PostsService: PostsService,public authService:AuthService) { }


  addBook(title_input:string,body_input:string,likes:number){
    this.likes=0;
    this.booksservice.addBook(this.userId,title_input,body_input,this.likes);

  }


  ngOnInit() {
    // return this.PostsService.getPosts()
    //   .subscribe(posts => this.posts$ = posts);

    this.authService.user.subscribe(
      user=>{
        this.userId=user.uid; 
      }
    )
    this.PostsService.getPosts()
      .subscribe(posts => this.posts$ = posts);

      this.PostsService.getComments()
      .subscribe(posts => this.comments$ = posts);
  }

  // ngOnInit() {
  //   this.posts$ = this.PostsService.getPosts();
  //   this.posts$.subscribe(data => {this.posts = data;
  //       console.log(this.posts);
  //     }
  //   ) 
  // }

}
