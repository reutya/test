import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Posts } from './interfaces/posts';
import { Comments } from './interfaces/comments';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiUrl = "https://jsonplaceholder.typicode.com/posts/";
  apiUrlComments="https://jsonplaceholder.typicode.com/comments/";

  constructor(private http:HttpClient,private db:AngularFirestore, private authService:AuthService) { }
  
  addPost(body:string, author:string,name:string){
    const post= {body:body, author:author, name:name};
    this.db.collection('posts').add(post);
  }
  

  // getPosts(){    
  //   return this.http.get<Posts>(this.apiUrl);
  // }

  
  getPosts(): Observable<Posts>{
    return this.http.get<Posts>(this.apiUrl)
    }

    getComments(){    
      return this.http.get<Comments>(this.apiUrlComments);
    }

}


